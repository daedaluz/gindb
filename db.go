package gindb

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"time"
)

// Connect connects to the database.
// This is a wrapper around sqlx.Connect.
func Connect(driver, dsn string) (*sqlx.DB, error) {
	return sqlx.Connect(driver, dsn)
}

// Open create the database object without connecting
func Open(driver, dsn string) (*sqlx.DB, error) {
	return sqlx.Open(driver, dsn)
}

var ErrTimeout = errors.New("timeout")

// WaitForDB waits for the database to be available with an optional timeout.
func WaitForDB(db *sqlx.DB, timeout time.Duration) error {
	if err := db.Ping(); err == nil {
		return nil
	}
	tick := time.NewTicker(time.Second)
	defer tick.Stop()
	if timeout == 0 {
		for _ = range tick.C {
			if err := db.Ping(); err == nil {
				break
			}
			time.Sleep(time.Second)
		}
	} else {
		start := time.Now()
		for t := range tick.C {
			if err := db.Ping(); err == nil {
				break
			}
			if t.Sub(start) > timeout {
				return ErrTimeout
			}
		}
	}
	return nil
}
