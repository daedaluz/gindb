package gindb

import (
	"github.com/jmoiron/sqlx"
	"io/fs"
	"os"
	"sort"
	"strings"
)

type migration struct {
	Name string
}

type fileSorter []os.DirEntry

func (f fileSorter) Len() int {
	return len(f)
}

func (f fileSorter) Less(i, j int) bool {
	return strings.Compare(f[i].Name(), f[j].Name()) < 0
}

func (f fileSorter) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

type migrationList []migration

// check if a migration has been Run
func (list migrationList) isRun(name string) bool {
	for _, m := range list {
		if m.Name == name {
			return true
		}
	}
	return false
}

// getMigrationFiles returns a list of migration files in correct order
func getMigrationFiles(migrations fs.ReadDirFS) []fs.DirEntry {
	// Get the list of migration files
	files, err := migrations.ReadDir(".")
	if err != nil {
		panic(err)
	}
	sort.Sort(fileSorter(files))
	// Sort the migration files in natural order
	// Return the sorted list of migration files
	return files
}

// getAlreadyRunMigrations returns a list of migrations that have already been Run
func getAlreadyRunMigrations(db *sqlx.DB) (migrationList, error) {
	res := make(migrationList, 0, 20)
	// Query the database for the list of already Run migrations
	if err := db.Select(&res, "SELECT name FROM migrations"); err != nil {
		return nil, err
	}
	return res, nil
}

// Migrate runs all files in migrations that have not already been Run.
// If the migrations table does not exist, it will be created and the migrations will be Run.
// Returns an error if any of the migrations fail and does a rollback.
func Migrate(db *sqlx.DB, migrations fs.ReadDirFS) error {
	migrationFiles := getMigrationFiles(migrations)
	alreadyRun, err := getAlreadyRunMigrations(db)
	if err != nil {
		// Migration table does not exist; create it and Run skeleton files
		if strings.Contains(err.Error(), "doesn't exist") {
			// Create the migrations table
			if _, err := db.Exec("CREATE TABLE migrations (Name VARCHAR(255) NOT NULL PRIMARY KEY, created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)"); err != nil {
				return err
			}
		} else {
			return err
		}
	}
	// Start a transaction
	tx := db.MustBegin()
	defer tx.Rollback()
	// For each migration file, if it has already been Run, skip it
	for _, file := range migrationFiles {
		if alreadyRun.isRun(file.Name()) {
			continue
		}
		sqlData, err := fs.ReadFile(migrations, file.Name())
		if err != nil {
			return err
		}
		if _, err := tx.Exec(string(sqlData)); err != nil {
			return err
		}
		if _, err := tx.Exec("INSERT INTO migrations (Name) VALUES (?)", file.Name()); err != nil {
			return err
		}
	}
	// Commit the transaction
	return tx.Commit()
}
