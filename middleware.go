package gindb

import (
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

// MiddlewareDB is a gin middleware that adds a *sqlx.DB to the context.
func MiddlewareDB(db *sqlx.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("*sqlx.DB", db)
	}
}

type TxErrorHandler func(c *gin.Context, err error)

// MiddlewareTX is a gin middleware that wraps the request in a transaction.
// If the request is successful, the transaction is committed. If the request
// fails or is aborted, the transaction is rolled back.
func MiddlewareTX(errorHandler TxErrorHandler) gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := BeginTx(c); err != nil {
			if errorHandler != nil {
				errorHandler(c, err)
			}
			return
		}
		defer func() {
			_ = Rollback(c)
		}()
		c.Next()
		if !c.IsAborted() {
			if err := Commit(c); err != nil {
				if errorHandler != nil {
					errorHandler(c, err)
				}
			}
		}
	}
}
