package gindb

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

// GetConnection returns the *sqlx.DB from the context.
func GetConnection(c *gin.Context) *sqlx.DB {
	return c.MustGet("*sqlx.DB").(*sqlx.DB)
}

// BeginTx starts a transaction in the context.
func BeginTx(c *gin.Context) error {
	tx, err := GetConnection(c).Beginx()
	if err != nil {
		return err
	}
	c.Set("*sqlx.Tx", tx)
	return nil
}

// BeginTxx starts a transaction in the context with options.
func BeginTxx(c *gin.Context, opts *sql.TxOptions) error {
	tx, err := GetConnection(c).BeginTxx(c, opts)
	if err != nil {
		return err
	}
	c.Set("*sqlx.Tx", tx)
	return nil
}

// GetTX returns the *sqlx.Tx from the context.
func GetTX(c *gin.Context) *sqlx.Tx {
	return c.MustGet("*sqlx.Tx").(*sqlx.Tx)
}

// Rollback rolls back the transaction in the context.
func Rollback(c *gin.Context) error {
	return GetTX(c).Rollback()
}

// Commit commits the transaction in the context.
func Commit(c *gin.Context) error {
	return GetTX(c).Commit()
}
